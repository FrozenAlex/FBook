package com.frozenalex.fbook

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.frozenalex.fbook.data.model.Book


class BookListAdapter(books: ArrayList<Book>) : RecyclerView.Adapter<BookListAdapter.ViewHolder>() {
    private val books: ArrayList<Book> = books
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    class ViewHolder(v: ConstraintLayout) : RecyclerView.ViewHolder(v) {
        var mItemDescription: TextView = v.findViewById(R.id.item_description)
        var mItemImage: ImageView = v.findViewById(R.id.item_image)
        var mItemDate: TextView = v.findViewById(R.id.item_date)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookListAdapter.ViewHolder {
        // create a new view
        var v = LayoutInflater.from(parent.context)
                .inflate(R.layout.booklistview_item, parent, false)
        // set the view's size, margins, paddings and layout parameters
        return ViewHolder(v as ConstraintLayout)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mItemDescription.setText(books[position].title)
        holder.mItemDate.setText(books[position].author)
        Glide.with(holder.mItemImage).load(books[position].thumbURL).into(holder.mItemImage)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        return books.size;
    }
}
