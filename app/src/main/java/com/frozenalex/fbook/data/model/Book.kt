package com.frozenalex.fbook.data.model

import android.os.Parcel
import android.os.Parcelable
import android.support.v7.widget.DialogTitle
import java.io.Serializable

/**
 * Created by Alex Uskov on 7/13/17.
 */

class Book(id: Long, title: String, author: String, description: String) : Serializable {

    var id: Long;
    var title: String;
    var author: String;
    var description: String

    var thumbnail_url: String? = null

    var source = ""

    init {
        this.id = id
        this.title = title
        this.author = author
        this.description = description
    }


    val thumbURL: String get() {
        val sub = id / 100;
        return "https://fantasy-worlds.org/img/preview/" + (id / 100) + "/" + id + ".jpg"
    }
    val coverURL: String get() {
        val sub = id / 100;
        return "https://fantasy-worlds.org/img/full/" + (id / 100) + "/" + id + ".jpg"
    }


    override fun hashCode(): Int {
        return id.toInt()
    }

    override fun equals(other: Any?): Boolean {
        if (this == other) return true
        if (this.id == (other as Book)?.id) return true
        return false
    }

    override fun toString(): String {
        return id.toString()
    }
}